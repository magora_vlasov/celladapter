package ru.whalemare.celladapter.base;

/**
 * Developed 2017.
 *
 * @author Valentin S.Bolkonsky
 */

public class BaseCellDelegateAdapter<T extends WrapperHolder>
        extends AbstractBaseCellDelegateAdapter<T, CellDelegate<T>> {
}
