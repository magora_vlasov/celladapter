package ru.whalemare.celladapter.cell

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * @since 2017
 * @author Anton Vlasov - whalemare
 */
abstract class DelegateCell<V : Cell.ViewHolder, in D : Any>(@LayoutRes val layoutRes: Int) {

    /**
     * Called to determine the correspondences between the data-object and the cell
     */
    abstract fun isViewType(item: Any): Boolean

    open class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        constructor(parent: ViewGroup, layoutRes: Int) :
                this(LayoutInflater.from(parent.context).inflate(layoutRes, parent, false))
    }

    abstract fun bind(holder: V, item: D)

    abstract fun viewHolder(parent: ViewGroup): V

    protected fun makeView(parent: ViewGroup): View {
        return LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
    }
}